package com.example.demo.entity;

import lombok.Data;

import java.util.*;

/**
 * 项目名称：test-qr-code
 * 类 名 称：Goal
 * 类 描 述：测试结构树
 * 创建时间：2020/11/20 9:17
 * 创 建 人：纪星亮
 */
@Data
public class Goal extends TreeNode {

    private String goalName;

    public List<Map> buildList(List<Map> list) {
        List<Map<Integer, List>> mapList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Map<Integer, List> map = list.get(i);
            Set<Integer> integers = map.keySet();
            for (Integer key : integers) {
                boolean flag = false;
                if (mapList.size() > 0) {
                    for (int x = 0; x < mapList.size(); x++) {
                        Map<Integer, List> m = mapList.get(x);
                        if (m.containsKey(key)) {
                            flag = true;
                            List list1 = m.get(key);
                            list1.add(map.get(key));
                        }
                    }
                    if (!flag) {
                        List<Object> objList = new ArrayList<>();
                        HashMap<Integer, List> obj = new HashMap<>();
                        obj.put(key, objList);
                        mapList.add(obj);
                    }
                } else {
                    List<Object> objList = new ArrayList<>();
                    HashMap<Integer, List> obj = new HashMap<>();
                    obj.put(key, objList);
                    mapList.add(obj);
                }
            }
        }
        return null;
    }



}