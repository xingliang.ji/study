package com.example.demo.entity;

import lombok.Data;

import java.util.Calendar;
import java.util.Date;

/**
 * 项目名称：spark
 * 类 名 称：DateFormatUtils
 * 类 描 述：TODO
 * 创建时间：2021/1/11 17:13
 * 创 建 人：纪星亮
 */
@Data
public class DateFormat {
    /**
     * 年份
     */
    private int year;
    /**
     * 月份
     */
    private int month;
    /**
     * 天
     */
    private int day;
    /**
     * 小时
     */
    private int hour;
    /**
     * 分钟
     */
    private int minute;
    /**
     * 秒
     */
    private int second;
    /**
     * 一周的第几天
     */
    private int WeekOfYear;

    /**
     * 初始化时间
     * @param date
     */
    public void setDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.year = cal.get(Calendar.YEAR);
        this.month=cal.get(Calendar.MONTH) + 1;
        this.day=cal.get(Calendar.DATE);
        this.hour=cal.get(Calendar.HOUR);
        this.minute=cal.get(Calendar.MINUTE);
        this.second=cal.get(Calendar.SECOND);
        this.WeekOfYear = cal.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 返回日期，格式到天
     * @return
     */
    public String getDate() {

        return this.year + "-" + this.month + "-" + this.day;
    }

    /**
     * 返回日期，格式到秒
     * @return
     */
    public String getDateTime() {

        return this.year + "-" + this.month + "-" + this.day + " " + this.hour + ":" + this.minute + ":" + this.second;
    }

    /**
     * 返回日期，格式到分钟
     * @return
     */
    public String getDateTimeTomMinute() {

        return this.year + "-" + this.month + "-" + this.day + " " + this.hour + ":" + this.minute;
    }

    /**
     * 返回日期，格式到月
     * @return
     */
    public String getDateToMonth() {

        return this.year + "-" + this.month;
    }
}