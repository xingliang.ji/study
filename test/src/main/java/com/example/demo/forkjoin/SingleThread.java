package com.example.demo.forkjoin;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name SingleThread
 * @description TODO
 * @date 2021/3/9 17:26
 */
@Service
public class SingleThread {
    @Resource
    private SqlTest sqlTest;

    public static final int SELECT_NUM = 5000;

    public void sum() {
        int minId = sqlTest.getMinId();
        int maxId = sqlTest.getMaxId();
        long sum = 0;


        /* 按照阈值计算第一次查询范围 */
        int fromId = minId;
        int toId = minId + SELECT_NUM;

        long startTime = System.currentTimeMillis();
        while ((toId - minId) <= maxId) {
            sum = sum + sqlTest.sumInfo(fromId, toId);

            fromId = (toId + 1);
            toId = (toId + SELECT_NUM);
        }

        System.out.println("统计总数 " + sum + "，一共用时" +(System.currentTimeMillis() - startTime) + "ms");
    }
}