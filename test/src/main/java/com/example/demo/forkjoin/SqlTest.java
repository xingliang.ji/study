package com.example.demo.forkjoin;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name SqlTest
 * @description TODO
 * @date 2021/3/9 16:42
 */
@Service
public class SqlTest {

    @Resource
    private SumUtil sumUtil;

    @Resource
    private TestInitArray testInitArray;

    /**
     * 模拟查询表中最小 id
     * @return
     */
    public int getMinId() {
        String sql = "select min(id) from order_info";
        Random r = new Random();
        return 1;
    }

    /**
     * 模拟查询表中最大 id
     * @return
     */
    public int getMaxId() {
        String sql = "select max(id) from order_info";
        Random r = new Random();
        return 1000000;
    }

    @SqlCount
    public long sumInfo(int mixId, int maxId) {
        long sum = 0;
        String sql = "select order_id_max from order_info where id >= " + mixId + " and id <= " + maxId;
//        System.out.println("minId = " + mixId + ", maxId = " + maxId);
        try {
            /* 代表实际查询数据库的过程 */
            Thread.sleep(15);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /* 随机 指定数量 值 模拟实际 id */
        int[] result = testInitArray.initArray(SingleThread.SELECT_NUM);
        return sumUtil.sum(result);
    }
}