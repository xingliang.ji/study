package com.example.demo.forkjoin;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name CountSqlSum
 * @description TODO
 * @date 2021/3/10 9:39
 */
public class CountSqlSum {
    public static long count = 0;

    public synchronized static void countSum() {
        count = (count + 1);
    }
}