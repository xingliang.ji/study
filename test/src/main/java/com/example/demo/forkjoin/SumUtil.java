package com.example.demo.forkjoin;

import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name SumUtil
 * @description TODO
 * @date 2021/3/9 17:14
 */
@Service
public class SumUtil {
    private int count = 0;
    public long sum(int[] info) {
        count ++;
        long sum = 0;
        for (int i = 0; i < info.length; i++) {
            sum = sum + info[i];
        }
        return sum;
    }
}