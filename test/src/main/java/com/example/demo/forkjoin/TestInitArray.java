package com.example.demo.forkjoin;

import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name TestInitArray
 * @description TODO
 * @date 2021/3/9 17:17
 */
@Service
public class TestInitArray {

    public int[] initArray(int num) {
        Random r = new Random();
        int[] info = new int[num];
        for (int i = 0; i < num; i++) {
            info[i] = r.nextInt(num);
        }
        return info;
    }
}