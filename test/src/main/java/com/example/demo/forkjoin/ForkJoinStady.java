package com.example.demo.forkjoin;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name ForkJoinStady
 * @description TODO
 * @date 2021/2/25 13:48
 */
@Service
public class ForkJoinStady {

    @Resource
    SqlTest sqlTest;

    private class MyTask extends RecursiveTask<Long> {
        private int formId;
        private int toId;
        private SqlTest sqlTest;

        public MyTask(int formId, int toId, SqlTest sqlTest) {
            this.formId = formId;
            this.toId = toId;
            this.sqlTest = sqlTest;
        }

        @Override
        protected Long compute() {

            // 如果数据长度符合 业务长度，执行业务逻辑
            if ((toId - formId) <= SingleThread.SELECT_NUM) {
                return sqlTest.sumInfo(formId, toId);
            } else {
                // 否则继续拆分，交给 ForkJoinPool 执行
                int median = (formId + toId) / 2;
                MyTask leftTask = new MyTask(formId, median, sqlTest);
                MyTask rightTask = new MyTask(median + 1, toId, sqlTest);

                invokeAll(leftTask, rightTask);

                return (leftTask.join() + rightTask.join());
            }
        }
    }

    public void sum() {
        ForkJoinPool fjp = new ForkJoinPool(
                Runtime.getRuntime().availableProcessors() * 18
        );

        int minId = sqlTest.getMinId();
        int maxId = sqlTest.getMaxId();
        MyTask myTask = new MyTask(minId, maxId, sqlTest);

        long startTime = System.currentTimeMillis();
        fjp.invoke(myTask);
        long endTime = System.currentTimeMillis();
        System.out.println("forkJoin结果 = " + myTask.join() + ",forkJoin执行时间 = " + (endTime - startTime) + "ms");
    }
}