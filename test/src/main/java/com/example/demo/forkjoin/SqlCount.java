package com.example.demo.forkjoin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name SqlCount
 * @description TODO
 * @date 2021/3/10 9:39
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SqlCount {
}
