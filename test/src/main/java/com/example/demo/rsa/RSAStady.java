package com.example.demo.rsa;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name RSATest
 * @description TODO
 * @date 2021/3/12 15:54
 */
@Component
public class RSAStady {

    /**
     * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 的那 两个 整数，并返回它们的数组下标。
     * <p>
     * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
     * <p>
     * 你可以按任意顺序返回答案。
     * <p>
     *  
     * <p>
     * 示例 1：
     * <p>
     * 输入：nums = [2,7,11,15], target = 9
     * 输出：[0,1]
     * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
     *
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        int index = nums.length;
        for (int i = 0; i < index; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if ((nums[i] + nums[j]) == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];
    }

    /**
     * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
     * <p>
     * 请你将两个数相加，并以相同形式返回一个表示和的链表。
     * <p>
     * 你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
     * <p>
     *  
     * <p>
     * 示例 1：
     * <p>
     * <p>
     * 输入：l1 = [2,4,3], l2 = [5,6,4]
     * 输出：[7,0,8]
     * 解释：342 + 465 = 807.
     *
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        /* 初始化链表对象，和链表子对象 */
        ListNode head = null, tail = null;
        /*
         * carry 进位值
         * n1,n2 两个链表同位置的值
         * sum 进位值 与 n1,n2 的和
         *  */
        int carry = 0;
        while (l1 != null || l2 != null) {
            int n1 = l1 != null ? l1.val : 0;
            int n2 = l2 != null ? l2.val : 0;
            int sum = n1 + n2 + carry;
            if (head == null) {
                head = tail = new ListNode(sum % 10);
            } else {
                tail.next = new ListNode(sum % 10);
                tail = tail.next;
            }
            carry = sum / 10;
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        if (carry > 0) {
            tail.next = new ListNode(carry);
        }
        return head;
    }

    /**
     * 给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
     * <p>
     *  
     * <p>
     * 示例 1:
     * <p>
     * 输入: s = "abcabcbb"
     * 输出: 3
     * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
     * 示例 2:
     * <p>
     * 输入: s = "bbbbb"
     * 输出: 1
     * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
     *
     * @param s
     * @return
     */
    public static int lengthOfLongestSubstring(String s) {
        int length = s.length();
        if (length > 1) {
            char[] chars = s.toCharArray();

            int num = chars.length;
            String temp = "";
            int index = -1;

            for (int i = 0; i < num; i++) {
                char aChar = chars[i];
                if (temp.indexOf(aChar) != -1) {
                    index = index > temp.length() ? index : temp.length();
                    temp = temp.substring(temp.indexOf(aChar) + 1);
                }
                temp += aChar;
            }
            return index > temp.length() ? index : temp.length();
        }
        return length;
    }

    /**
     * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
     * <p>
     *  
     * <p>
     * 示例 1：
     * <p>
     * 输入：nums1 = [1,3], nums2 = [2]
     * 输出：2.00000
     * 解释：合并数组 = [1,2,3] ，中位数 2
     * 示例 2：
     * <p>
     * 输入：nums1 = [1,2], nums2 = [3,4]
     * 输出：2.50000
     * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] nums = marge(nums1, nums2);
        int length = nums.length;

        if (length % 2 == 1) {
            return nums[length / 2];
        } else {
            return (nums[(length / 2) - 1] + nums[length / 2]) / 2.0;
        }
    }

    private static int[] marge(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        int i = 0, j = 0;
        int index = 0;
        //比较指针i,j指向的值，小的值存入指针index指向的结果数组中，当有一个指针（i或j）先到达数组末尾时，比较结束；
        while (i < a.length && j < b.length) {
            if (a[i] < b[j]) {
                c[index++] = a[i++];
            } else {
                c[index++] = b[j++];
            }
        }
        int l;
        //将指针（i或j）没有到达数组末尾的数组复制到指针index指向的结果数组中
        if (i < a.length) {
            for (l = i; l < a.length; l++) {
                c[index++] = a[l];
            }
        }
        //将指针（i或j）没有到达数组末尾的数组复制到指针index指向的结果数组中
        if (j < b.length) {
            for (l = j; l < b.length; l++) {
                c[index++] = b[l];
            }
        }
        return c;
    }

    /**
     * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
     *
     * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
     *
     * 假设环境不允许存储 64 位整数（有符号或无符号）。
     *  
     *
     * 示例 1：
     *
     * 输入：x = 123
     * 输出：321
     * 示例 2：
     *
     * 输入：x = -123
     * 输出：-321
     * 示例 3：
     *
     * 输入：x = 120
     * 输出：21
     * @param x
     * @return
     */
    public static int numReverse(int x) {
        int res = 0;
        while(x!=0) {
            //每次取末尾数字
            int tmp = x%10;
            //判断是否 大于 最大32位整数
            if (res>214748364 || (res==214748364 && tmp>7)) {
                return 0;
            }
            //判断是否 小于 最小32位整数
            if (res<-214748364 || (res==-214748364 && tmp<-8)) {
                return 0;
            }
            res = res*10 + tmp;
            x /= 10;
        }
        return res;
    }
    /**
     * 超大数计算
     * @param num1
     * @param num2
     * @return
     */
    public static String twoLongNumAsSum(String num1, String num2) {
        /* 当有一个数为空时，直接返回另一个 */
        if (null == num1 || "".equals(num1.trim())) {
            return num2;
        }

        if (null == num2 || "".equals(num2.trim())) {
            return num1;
        }

        StringBuffer result = new StringBuffer();

        /* 转换为 char 数组，从个位开始计算 */
        char[] arr1 = num1.trim().toCharArray();
        char[] arr2 = num2.trim().toCharArray();

        /* index1,index2 两个数的个位索引  carry 上一次计算是否需要进位 */
        int index1 = arr1.length - 1;
        int index2 = arr2.length - 1;
        boolean carry = false;

        /* 循环两数的值，丛个位开始计算，计算到任意一个数的最高位停止计算 */
        while (index1 >= 0 && index2 >= 0) {
            int i = arr1[index1] - '0';
            int j = arr2[index2] - '0';
            int sum = i + j;

            /* 是否有进位，如果有本次计算结果加一 */
            sum  = carry ? sum + 1 : sum;

            /* 本次计算结果大于 10 进位标识为 true */
            carry = sum >= 10 ? true : false;

            result.append((char)(sum % 10 + '0'));

            index1 --;
            index2 --;
        }

        /* 处理两个数未计算的位数 */
        while (index1 >= 0) {
            // 处理当前元素
            int i = arr1[index1] - '0';
            // 是否需要进位
            i = carry ? i + 1 : i;
            // 下次是否需要进位
            carry = i >= 10 ? true : false;
            // 添加结果
            result.append((char)(i % 10 + '0'));

            index1 --;
        }
        while (index2 >= 0) {
            int i = arr2[index2] - '0';

            i = carry ? i + 1 : i;

            carry = i >= 10 ? true : false;

            result.append((char)(i % 10 + '0'));

            index2 --;
        }

        /* 是否有进位 */
        if (carry) {
            result.append(1);
        }

        // 反转结果返回
        return result.reverse().toString();
    }

    public static int hammingWeight(int n) {
        /* 计数器 */
        int num = 0;

        for (int i = 0; i < 32; i++) {
            if ((n & (1 << i)) != 0) {
                num ++;
            }
        }
        return num;
    }

    public static void rotate(int[][] matrix) {
        int[][] info = new int[matrix.length][matrix[0].length];
        for (int j = 0; j < matrix[0].length; j++) {
            int index = matrix.length - 1;
            int x = 0;
            while (index >= 0) {
                info[j][x++] = matrix[index--][j];
            }
        }
        System.out.println(JSON.toJSONString(info));
    }

    public static void main(String[] args) {
        Integer a = Integer.valueOf("100000");
        int b = 100000;

        System.out.println(a == b);

        Integer c = 1000;
        Integer d = 1000;
        System.out.println(c == d);

        Integer e = 100;
        Integer f = 100;
        System.out.println(e == f);

        String s2 = "a" + "b";
        String s3 = "a";
        String s4 = "b";
        String s5 = s3 + s4;
        String s6 = "ab";
        System.out.println(s2 == s5);
        System.out.println(s2 == s6);

        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        list.add(15);
        list.add(26);
        list.add(6);
        list.add(14);
        list.add(33);
        deleteList(list);
    }

    private static String initStringNum(int num) {
        Random r = new Random();
        String s = "";
        for (int i = 0; i < num; i++) {
            s += r.nextInt(10);
        }
        return s;
    }

    public static void deleteList(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            Integer x = list.get(i);
            if (x > 10) {
                list.remove(i);
                i--;
            }
        }

        System.out.println(JSON.toJSONString(list));
    }
}