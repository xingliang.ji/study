package com.example.demo.test;

import com.example.demo.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 项目名称：test-qr-code
 * 类 名 称：CodeTest
 * 类 描 述：TODO
 * 创建时间：2020/11/5 13:11
 * 创 建 人：纪星亮
 */
@RestController
public class CodeTest {

    @GetMapping("testGetUser")
    public User testGetUser(User user) {
        return user;
    }
    //    @GetMapping("/testNameEquls")
//    public void testNameEquls(String testName) {
//        String name = "张三";
//        System.out.println(name == testName);
//        System.out.println("testName hashCode = " + System.identityHashCode(testName));
//        System.out.println("name hashCode = " + System.identityHashCode(name));
//    }

    /**
     * -Xms30m -Xm×30m -Xss1m -XX:MaxMetaspaceSize=30m -XX :+UseConcMarkSweepGC -XX:-UseCompressed0ops
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
//        boolean flag = false;
//        changeFlag(flag);
//        System.out.println("flag = " + flag);
//
//        User user = new User();
//        changUser(user);
//        String s2 = new String("张") + new String("三");
//        String s3 = s2.intern();
//        String s1 = "张三";
//        System.out.println(System.identityHashCode(s1));
//        System.out.println(System.identityHashCode(s2));
//        System.out.println(System.identityHashCode(s3));
//        System.out.println(s1 == s2);
//        System.out.println(s1 == s3);
//        System.out.println(s2 == s3);
        int[] info = {1, 3, 9, 16, 10};


        int[] ints = twoSum(info, 10);

        System.out.println(ints);
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] index = new int[2];
        for (int i = 0; i < nums.length; i ++) {
            int x = nums[i];
            for (int j = i + 1; j < nums.length; j ++) {
                int y = nums[j];
                int sum = x + y;
                if (sum == target) {
                    index[0]= i > j ? j : i;
                    index[1] = i > j ? i : j;
                    return index;
                }
            }
        }
        return null;
    }

    public static void changUser(User user) {
        user = null;
    }


    public static void changeFlag(boolean flag) {
        flag = true;
    }
}