package com.example.demo.test;

import com.deepoove.poi.XWPFTemplate;
import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 项目名称：test-qr-code
 * 类 名 称：GenerateWord
 * 类 描 述：TODO
 * 创建时间：2020/12/11 14:05
 * 创 建 人：纪星亮
 */
public class GenerateWord {

    private static final String DOC = "doc";

    private static final String PDF = "pdf";

    public static Map<String, Object> GenerateWordPdf(Map map, String modelName) {
        return GenerateWordPdf(map, modelName, true);
    }

    /**
     *
     * @param map   word 文档占位符参数集合
     * @param modelName    项目资源文件夹中对应 docx 文件名称
     * @param isGeneratePdf 是否生成 pdf 文件
     * @return {
     *    pdf={
     *      fileName=M00/00/00/rBIYgV_gH2uARrTdAAF-KwuIaWo972.pdf,
     *      fileSize=97835,   文件大小
     *      filePath=M00/00/00/rBIYgV_gH2uARrTdAAF-KwuIaWo972.pdf,  fastDfs 服务器路径
     *      resCode=200,    状态码
     *      resMsg=M00/00/00/rBIYgV_gH2uARrTdAAF-KwuIaWo972.pdf,
     *      ViewName=doc.pdf
     *    },
     *    doc={
     *      fileName=M00/00/00/rBIYgV_gH2eADt1xAADaIpVjSeA19.docx,
     *      fileSize=55842,
     *      filePath=M00/00/00/rBIYgV_gH2eADt1xAADaIpVjSeA19.docx,
     *      resCode=200,
     *      resMsg=M00/00/00/rBIYgV_gH2eADt1xAADaIpVjSeA19.docx,
     *      ViewName=doc.docx
     *    }
     * }
     */
    public static Map<String, Object> GenerateWordPdf(Map map, String modelName, boolean isGeneratePdf) {
        Map<String, Object> fileInfoMap = new HashMap<>();
        ClassPathResource docSource = new ClassPathResource("file\\doc.docx");
        ClassPathResource pdfSource = new ClassPathResource("file\\doc.pdf");

        InputStream is = null;
        XWPFTemplate template = null;
        FileOutputStream out = null;
        OutputStream outputStream1 = null;
        InputStream docxInputStream = null;
        IConverter converter = null;
        try  {
            // 获取项目资源文件夹中的指定模板文件
            ClassPathResource resource = new ClassPathResource("doc\\"+modelName+".docx");
            if (resource.exists()) {
                // 获取文件输入流
                is = resource.getInputStream();

                // 替换 word 模板文件中的占位符，并保存到本地磁盘
                template = XWPFTemplate.compile(is).render(map);
                File docFile = docSource.getFile();
                out = new FileOutputStream(docFile);
                template.write(out);
                out.flush();

                // 文件保存成功后，上传到 fastDFS 服务器
                if (docFile.exists()) {
                    Map<String, Object> upload = FastDfsUtil.upload(docFile.getPath());
                    fileInfoMap.put(DOC, upload);
                }

                if (isGeneratePdf) {
                    // 获取替换完成后的 word 文件，转换为 PDF 后，上传到 fastDFS 服务器
                    File pdfFile = pdfSource.getFile();
                    outputStream1 = new FileOutputStream(pdfFile);
//                  ServletOutputStream outputStream1 = response.getOutputStream();

                    docxInputStream = new FileInputStream(docFile);
                    converter = LocalConverter.builder().build();
                    converter.convert(docxInputStream).as(DocumentType.DOCX).to(outputStream1).as(DocumentType.PDF).execute();

                    if (pdfFile.exists()) {
                        Map<String, Object> upload = FastDfsUtil.upload(pdfFile.getPath());
                        fileInfoMap.put(PDF, upload);
                    }
                }
//                System.out.println("success");
                return fileInfoMap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != docxInputStream)
                    converter.kill();

                if (null != docxInputStream)
                    docxInputStream.close();

                if (null != outputStream1)
                    outputStream1.close();

                if (null != out)
                    out.close();

                if (null != template)
                    template.close();

                if (null != is)
                    is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new HashMap<>();
    }

    public static void main(String[] args) throws IOException {
        Map map = new HashMap(){{
            put("year", "2020");
            put("jname", "小亮甲");
            put("month", "09");
            put("day", "01");
            put("yname", "小亮亿");
        }};

//        Map<String, Object> yxs = GenerateWordPdf(map, "yxs", true);
//        System.out.println(yxs.toString());
    }
}