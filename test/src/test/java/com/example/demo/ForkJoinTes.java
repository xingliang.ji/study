package com.example.demo;

import com.example.demo.forkjoin.CountSqlSum;
import com.example.demo.forkjoin.ForkJoinStady;
import com.example.demo.forkjoin.SingleThread;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @project_name test-qr-code
 * @name ForkJoinTes
 * @description TODO
 * @date 2021/3/9 17:39
 */
@SpringBootTest
public class ForkJoinTes {
    @Resource
    SingleThread singleThread;

    @Resource
    ForkJoinStady forkJoinStady;

    @Test
    void testSt() {
        singleThread.sum();
        System.out.println("次数 = " + CountSqlSum.count);
        System.out.println("-------------------------------------------");
        CountSqlSum.count = 0;
        forkJoinStady.sum();
        System.out.println("次数 = " + CountSqlSum.count);
    }

    @Test
    void testForkJoin() {
        forkJoinStady.sum();
    }
}