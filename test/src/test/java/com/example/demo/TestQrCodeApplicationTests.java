package com.example.demo;

import com.example.demo.aop.TestAop.AopService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TestQrCodeApplicationTests {

	@Resource
	AopService aopService;
	@Test
	public void a() {
		aopService.findById("123");
	}

}
